import struct


a = b"hello"
b = b"world"
c = 20
d = 42.56

binStr = struct.pack("5s6sif",a,b,c,d)
print(len(binStr))
binStr2 = struct.pack("i",c)

e,f,g,h = struct.unpack("5s6sif",binStr)
print(e,f,g,h)

i, = struct.unpack("i",binStr2)
print(i)

i = struct.unpack("i",binStr2)
print(i)

print(struct.calcsize("5s6sif"))

f = b"\x00\x01\x02\x03\x30"
print(f)